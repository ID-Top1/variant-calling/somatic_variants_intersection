#!/bin/bash

usage=$(cat <<EOT
Usage: $0 [options] tumour_normal_pairs.txt ref.fasta

Options:

    tumour_normal_pairs.txt
        Tab separated file with tumour normal and sample IDs as first 3 columns.
        Required.

    ref.fasta
        FASTA reference file matching your VCF input.

    -d <results dir>
        Directory containing output from somatic variant calling. If not
        provided it is assumed to be the same directory as your
        tumour_normal_pairs.txt file.

    -h
        Show this message and exit.

EOT
)

while getopts 'hd:' opt; do
    case "$opt" in
        h)
            echo -e "$usage"
            exit
            ;;
        d)
            dir=${OPTARG}
            ;;
        ?)
            echo -e "$usage";
            exit 1
            ;;
        :)
            echo "Invalid option: $OPTARG requires an argument" 1>&2
            ;;
    esac
done

shift "$(($OPTIND -1))"

if [ $# -ne 2 ]
then
    if [ $# -lt 2 ]
    then
        echo -e "Please supply tumour_normal_pairs.txt and ref_fasta arguments\n"
    elif [ $# -gt 2 ]
    then
        echo -e "Trailing arguments\n"
    fi
    echo -e "$usage"
    exit 1
fi


pairs=$1
ref=$2

if [ -z "$dir" ]
then
    dir=$(dirname $pairs)
fi
set -euo pipefail




echo $(date) Renaming samples and normalizing SVABA VCFs
for vcf in ${dir}/svaba_somatic_indels/*/*.svaba.somatic.indel.vcf.gz
do
    renamed=$(dirname $vcf)/$(basename $vcf .vcf.gz).renamed.norm.vcf.gz
    bcftools reheader -s <(for sample in `bcftools query -l $vcf `;
                           do
                               basename $sample .mkdups.bqsr.bam;
                           done ) $vcf | \
        bcftools norm -f $ref -cw -O z -o $renamed
done

plty_vcf=${dir}/platypus_output/all_samples.platypus.sorted.norm.vcf.gz
plty_flt=$(dirname $plty_vcf)/$(basename ${plty_vcf} .vcf.gz).ad2_pass.bcf

echo $(date) Getting potential somatic variants from Platypus calls

bcftools reheader -h <(bcftools view -h $plty_vcf | \
        sed -e s/##FORMAT=\<ID=NV,Number=./##FORMAT=\<ID=NV,Number=A/ \
            -e s/##FORMAT=\<ID=NR,Number=./##FORMAT=\<ID=NR,Number=A/ \
            -e s/\.mkdups\.bqsr//g ) $plty_vcf | \
    bcftools view -O u -e 'FILTER ~ "QD" | FILTER ~ "GOF" | 
    FILTER ~ "badReads" | FILTER ~ "hp10" | FILTER ~ "Q20" | 
    FILTER ~ "HapScore" | FILTER ~ "MQ" | FILTER ~ "strandBias" | 
    FILTER ~ "QualDepth" | FILTER ~ "REFCALL" | FILTER ~ "QD"' | \
    cafex - \
     --case $(cut -f 1 $pairs) \
     --control $(cut -f 2 $pairs) \
     --ignore_genotypes \
     --case_expressions "NV > 2 && NR > 9" \
     --control_expressions "NV < 2 all" "NR > 19" \
     --vaf_ratio 10 \
     -o $plty_flt

echo $(date) Separating Platypus calls
pps_dir=${dir}/platypus_output/per_sample_somatic
mkdir -p $pps_dir
while read -r tumour normal sample
do
    out=${pps_dir}/${sample}.norm.ad2_pass.vaf_ratio_10.vcf.gz
    bcftools view -O u -s ${normal},${tumour} $plty_flt | \
        cafex - \
        --ignore_genotypes \
        --case $tumour \
        --control $normal \
        --case_expressions "NV > 2 && NR > 9" \
        --control_expressions "NV < 2 and NR > 19" \
        --vaf_ratio 10 \
        -o $out
    tabix -p vcf ${out}
done < $pairs

echo $(date) Done
echo $?
