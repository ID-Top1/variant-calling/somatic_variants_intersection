# Normalization and intersection of somatic variant calls

## Required Software

* [bcftools](http://www.htslib.org/download/)
* [tabix](http://www.htslib.org/download/)
* [cafex](https://github.com/david-a-parry/cafex)


## Running

Following completion of somatic_variant_calling_template or
mouseRnaseh2KoTumoursPipeline, these wrapper scripts will perform variant
normalization and intersection per-sample.

Requires the tumour normal pairs table provided to the variant calling pipeline
and the location of your variant calling results directory.

~~~

./pre_processing.sh tumour_normal_pairs.txt \
    /path/to/ref.fasta \
    -d /path/to/variant_calling_output

./intersect_variants.sh -t tumour_normal_pairs.txt \
    -d /path/to/variant_calling_output/
    -m 3 \
    -o /path/to/intersected_variant_output

~~~

## AUTHOR

Written by David A. Parry at the University of Edinburgh.
