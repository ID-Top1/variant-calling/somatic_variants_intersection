#!/bin/bash

usage=$(cat <<EOT
Usage: $(basename $0) -t <tumour_normal_pairs.txt> -m <min_intersects> -d <results dir> -o <output dir> [-e <extra args>]

Options:

    -t <tumour_normal_pairs.txt>
        Tab separated file with tumour normal and sample IDs as first 3 columns.
        Required.

    -d <results dir>
        Directory containing output from somatic variant calling. Required.

    -m <N>
        Minimum number of intersections required. Required.

    -o <output dir>
        Output directory. Full path will be created. Program exits if this
        directory already exists.

    -e <extra args>
        Extra arguments for bcftools. Can be specified multiple times.

    -h
        Show this message and exit.

EOT
)


while getopts 'ht:d:m:o:e::' opt; do
    case "$opt" in
        h)
            echo -e "$usage"
            exit
            ;;
        t)
            pairs=${OPTARG}
            ;;
        d)
            dir=${OPTARG}
            ;;
        m)
            min_intersects=${OPTARG}
            if [[ ! $min_intersects =~ ^[0-9]+$ ]]
            then
                echo "-m argument must be a non-negative integer - exiting"
                exit 2
            fi
            ;;
        o)
            outdir=${OPTARG}
            ;;
        e)
            extra_args=${OPTARG}
            ;; # e.g. '-f PASS' to only intersect variants with PASS in FILTER field
        ?)
            echo -e "$usage";
            exit 1
            ;;
        :) 
            echo "Invalid option: $OPTARG requires an argument" 1>&2
            ;;
    esac
done

if ((OPTIND == 1))
then
    echo -e "$usage"
    exit 1
fi

if [ -z "$pairs" ] || [ -z "$dir" ] || [ -z "$min_intersects" ] || [ -z "$outdir" ]; then
    echo -e "Missing argument\n"
    echo -e "$usage"
    exit 1
fi

if [ -z "$extra_args" ]
then
    extra_args=""
fi

set -eou pipefail


shift "$(($OPTIND -1))"


if [ -e "$outdir" ]
then
    echo "Error: Output directory $outdir already exists - exiting"
    exit 3
fi

mkdir -p ${outdir}

for sample in `cut -f 3 $pairs`
do
    echo $(date) Processing ${sample}
    bcftools isec -p ${outdir}/${sample} -n +${min_intersects} ${extra_args} \
        ${dir}/mutect2_somatic_calls/${sample}/${sample}.somatic.filtered_no_contam.vcf.gz \
        ${dir}/strelka_calls/${sample}/results/variants/somatic.indels.vcf.gz \
        ${dir}/strelka_calls/${sample}/results/variants/somatic.snvs.vcf.gz \
        ${dir}/svaba_somatic_indels/${sample}/${sample}.svaba.somatic.indel.renamed.norm.vcf.gz \
        ${dir}/platypus_output/per_sample_somatic/${sample}.norm.dp20_ad2_pass.vcf.gz

    for vcf in  ${outdir}/${sample}/*vcf 
    do 
        bgzip -f ${vcf}
        tabix -fp vcf ${vcf}.gz
        bcftools annotate -x FORMAT -O z -o ${vcf}.cleaned.vcf.gz ${vcf}.gz 
        tabix -p vcf ${vcf}.cleaned.vcf.gz
    done
    bcftools merge ${outdir}/${sample}/*cleaned.vcf.gz --force-samples | \
        bcftools view -G -O v -o ${outdir}/${sample}.vcf
done

